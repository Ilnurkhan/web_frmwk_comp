from aiohttp import web
import asyncio
import uvloop
import uuid

asyncio.set_event_loop_policy(uvloop.EventLoopPolicy())

async def handle(request):
    text = uuid.uuid4().hex
    return web.Response(text=text, headers={})

app = web.Application()
app.add_routes([web.get('/', handle)])

web.run_app(app)
