import flask
import uuid
app = flask.Flask(__name__)

@app.route("/")
def hello():
    resp = flask.Response(uuid.uuid4().hex)
    resp.headers['Date']='Mon, 03 Sep 2018 19:04:35 GMT'
    resp.headers['Server']='Python/3.6 aiohttp/3.4.2'
    return resp


application = app#.run('0.0.0.0')
