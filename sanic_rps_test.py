from sanic import Sanic
from sanic import response
from sanic.response import json
import uuid

app = Sanic()

@app.route('/')
async def test(request):
    return response.text(uuid.uuid4().hex)

if __name__ == '__main__':
    app.run(host='0.0.0.0', port=9000)
